package com.rifkaannisa.app.ui.splash.interactor

import com.rifkaannisa.app.data.database.repository.questions.Question
import com.rifkaannisa.app.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by jyotidubey on 04/01/18.
 */
interface SplashMVPInteractor : MVPInteractor {

    fun seedQuestions(): Observable<Boolean>
    fun seedOptions(): Observable<Boolean>
    fun getQuestion() : Observable<List<Question>>
}