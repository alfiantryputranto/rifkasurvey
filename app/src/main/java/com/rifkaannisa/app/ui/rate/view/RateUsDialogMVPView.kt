package com.rifkaannisa.app.ui.rate.view

import com.rifkaannisa.app.ui.base.view.MVPView

/**
 * Created by jyotidubey on 14/01/18.
 */
interface RateUsDialogMVPView : MVPView{

    fun dismissDialog()
    fun showRatingSubmissionSuccessMessage()
}