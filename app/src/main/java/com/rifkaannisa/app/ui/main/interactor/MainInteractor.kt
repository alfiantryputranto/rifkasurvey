package com.rifkaannisa.app.ui.main.interactor

import com.rifkaannisa.app.data.database.repository.options.Options
import com.rifkaannisa.app.data.database.repository.options.OptionsRepo
import com.rifkaannisa.app.data.database.repository.questions.Question
import com.rifkaannisa.app.data.database.repository.questions.QuestionRepo
import com.rifkaannisa.app.data.network.ApiHelper
import com.rifkaannisa.app.data.preferences.PreferenceHelper
import com.rifkaannisa.app.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by jyotidubey on 08/01/18.
 */
class MainInteractor @Inject internal constructor(private val questionRepoHelper: QuestionRepo, private val optionsRepoHelper: OptionsRepo, preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), MainMVPInteractor {

    override fun getQuestionCardData() = questionRepoHelper.loadQuestions()
            .flatMapIterable { question -> question }
            .flatMapSingle { question -> getQuestionCards(question) }
            .toList()

    override fun getUserDetails() = Pair(preferenceHelper.getCurrentUserName(),
            preferenceHelper.getCurrentUserEmail())

    override fun makeLogoutApiCall() = apiHelper.performLogoutApiCall()

    private fun getQuestionCards(question: Question) = optionsRepoHelper.loadOptions(question.id)
            .map { options -> createQuestionCard(options, question) }

    private fun createQuestionCard(options: List<Options>, question: Question) = QuestionCardData(options, question)

}


