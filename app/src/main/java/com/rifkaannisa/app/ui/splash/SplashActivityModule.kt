package com.rifkaannisa.app.ui.splash

import com.rifkaannisa.app.ui.splash.interactor.SplashInteractor
import com.rifkaannisa.app.ui.splash.interactor.SplashMVPInteractor
import com.rifkaannisa.app.ui.splash.presenter.SplashMVPPresenter
import com.rifkaannisa.app.ui.splash.presenter.SplashPresenter
import com.rifkaannisa.app.ui.splash.view.SplashMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by jyotidubey on 06/01/18.
 */
@Module
class SplashActivityModule {

    @Provides
    internal fun provideSplashInteractor(splashInteractor: SplashInteractor): SplashMVPInteractor = splashInteractor

    @Provides
    internal fun provideSplashPresenter(splashPresenter: SplashPresenter<SplashMVPView, SplashMVPInteractor>)
            : SplashMVPPresenter<SplashMVPView, SplashMVPInteractor> = splashPresenter
}