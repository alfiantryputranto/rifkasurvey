package com.rifkaannisa.app.ui.login.interactor

import com.rifkaannisa.app.data.network.ApiHelper
import com.rifkaannisa.app.data.network.LoginRequest
import com.rifkaannisa.app.data.network.LoginResponse
import com.rifkaannisa.app.data.preferences.PreferenceHelper
import com.rifkaannisa.app.ui.base.interactor.BaseInteractor
import com.rifkaannisa.app.util.AppConstants
import javax.inject.Inject
import kotlin.math.log

/**
 * Created by jyotidubey on 10/01/18.
 */
class LoginInteractor @Inject internal constructor(
  preferenceHelper: PreferenceHelper,
  apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper, apiHelper), LoginMVPInteractor {

  override fun doServerLoginApiCall(
    username: String,
    password: String
  ) =
    apiHelper.performServerLogin(
        LoginRequest.ServerLoginRequest(
            username = username, password = password
        )
    )

  override fun updateUserInSharedPref(
    loginResponse: LoginResponse,
    loggedInMode: AppConstants.LoggedInMode
  ) =
    preferenceHelper.let {
      it.setCurrentUserId(loginResponse.userId)
      it.setCurrentUserName(loginResponse.userName)
      it.setCurrentUserEmail(loginResponse.email)
      it.setCurrentUserFullName(loginResponse.fullName)
      it.setCurrentUserRole(loginResponse.role)
      it.setCurrentUserLembaga(loginResponse.lembaga)
      it.setAccessToken(loginResponse.accessToken)
      it.setCurrentUserLoggedInMode(loggedInMode)
    }
}