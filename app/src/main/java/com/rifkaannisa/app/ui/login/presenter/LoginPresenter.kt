package com.rifkaannisa.app.ui.login.presenter

import com.rifkaannisa.app.data.network.LoginResponse
import com.rifkaannisa.app.ui.base.presenter.BasePresenter
import com.rifkaannisa.app.ui.login.interactor.LoginMVPInteractor
import com.rifkaannisa.app.ui.login.view.LoginMVPView
import com.rifkaannisa.app.util.AppConstants
import com.rifkaannisa.app.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import javax.inject.Inject

/**
 * Created by jyotidubey on 10/01/18.
 */
class LoginPresenter<V : LoginMVPView, I : LoginMVPInteractor> @Inject internal constructor(
  interactor: I,
  schedulerProvider: SchedulerProvider,
  disposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable
), LoginMVPPresenter<V, I> {

  override fun onServerLoginClicked(
    username: String,
    password: String
  ) {
    when {
      username.isEmpty() -> getView()?.showValidationMessage(AppConstants.EMPTY_USERNAME_ERROR)
      password.isEmpty() -> getView()?.showValidationMessage(AppConstants.EMPTY_PASSWORD_ERROR)
      else -> {
        getView()?.showProgress()
        interactor?.let {
          compositeDisposable.add(it.doServerLoginApiCall(username, password)
              .compose(schedulerProvider.ioToMainObservableScheduler())
              .doOnComplete { getView()?.hideProgress() }
              .subscribe({ loginResponse ->
                updateUserInSharedPref(
                    loginResponse = loginResponse,
                    loggedInMode = AppConstants.LoggedInMode.LOGGED_IN_MODE_SERVER
                )
                getView()?.openMainActivity()
              }, { err -> println(err) })
          )
        }

      }
    }
  }

  private fun updateUserInSharedPref(
    loginResponse: LoginResponse,
    loggedInMode: AppConstants.LoggedInMode
  ) =
    interactor?.updateUserInSharedPref(loginResponse, loggedInMode)

}