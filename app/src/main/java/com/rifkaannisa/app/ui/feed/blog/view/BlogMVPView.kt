package com.rifkaannisa.app.ui.feed.blog.view

import com.rifkaannisa.app.data.network.Blog
import com.rifkaannisa.app.ui.base.view.MVPView

/**
 * Created by jyotidubey on 13/01/18.
 */
interface BlogMVPView : MVPView {

    fun displayBlogList(blogs: List<Blog>?) : Unit?

}