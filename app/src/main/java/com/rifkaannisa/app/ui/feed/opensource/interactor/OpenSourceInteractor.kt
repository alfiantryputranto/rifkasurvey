package com.rifkaannisa.app.ui.feed.opensource.interactor

import com.rifkaannisa.app.data.network.ApiHelper
import com.rifkaannisa.app.data.network.OpenSourceResponse
import com.rifkaannisa.app.data.preferences.PreferenceHelper
import com.rifkaannisa.app.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by jyotidubey on 14/01/18.
 */
class OpenSourceInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), OpenSourceMVPInteractor {

    override fun getOpenSourceList(): Observable<OpenSourceResponse> {
        return apiHelper.getOpenSourceApiCall()
    }

}