package com.rifkaannisa.app.ui.feed.blog.interactor

import com.rifkaannisa.app.data.network.BlogResponse
import com.rifkaannisa.app.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by jyotidubey on 13/01/18.
 */
interface BlogMVPInteractor : MVPInteractor {

    fun getBlogList(): Observable<BlogResponse>

}