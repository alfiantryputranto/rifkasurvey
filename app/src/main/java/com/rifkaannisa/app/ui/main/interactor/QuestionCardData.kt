package com.rifkaannisa.app.ui.main.interactor

import com.rifkaannisa.app.data.database.repository.options.Options
import com.rifkaannisa.app.data.database.repository.questions.Question

/**
 * Created by jyotidubey on 08/01/18.
 */
data class QuestionCardData(val option: List<Options>, val question: Question)