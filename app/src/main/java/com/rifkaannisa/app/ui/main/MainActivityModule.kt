package com.rifkaannisa.app.ui.main

import com.rifkaannisa.app.ui.main.interactor.MainInteractor
import com.rifkaannisa.app.ui.main.interactor.MainMVPInteractor
import com.rifkaannisa.app.ui.main.presenter.MainMVPPresenter
import com.rifkaannisa.app.ui.main.presenter.MainPresenter
import com.rifkaannisa.app.ui.main.view.MainMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by jyotidubey on 09/01/18.
 */
@Module
class MainActivityModule {

    @Provides
    internal fun provideMainInteractor(mainInteractor: MainInteractor): MainMVPInteractor = mainInteractor

    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenter<MainMVPView, MainMVPInteractor>)
            : MainMVPPresenter<MainMVPView, MainMVPInteractor> = mainPresenter

}