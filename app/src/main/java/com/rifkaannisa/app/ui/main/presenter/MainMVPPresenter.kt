package com.rifkaannisa.app.ui.main.presenter

import com.rifkaannisa.app.ui.base.presenter.MVPPresenter
import com.rifkaannisa.app.ui.main.interactor.MainMVPInteractor
import com.rifkaannisa.app.ui.main.view.MainMVPView

/**
 * Created by jyotidubey on 08/01/18.
 */
interface MainMVPPresenter<V : MainMVPView, I : MainMVPInteractor> : MVPPresenter<V, I> {

    fun refreshQuestionCards(): Boolean?
    fun onDrawerOptionAboutClick() : Unit?
    fun onDrawerOptionRateUsClick(): Unit?
    fun onDrawerOptionFeedClick(): Unit?
    fun onDrawerOptionLogoutClick()

}