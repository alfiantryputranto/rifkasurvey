package com.rifkaannisa.app.ui.rate.interactor

import com.rifkaannisa.app.ui.base.interactor.MVPInteractor

/**
 * Created by jyotidubey on 15/01/18.
 */
interface RateUsMVPInterator : MVPInteractor{

    fun submitRating()
}