package com.rifkaannisa.app.ui.feed.blog.presenter

import com.rifkaannisa.app.ui.base.presenter.MVPPresenter
import com.rifkaannisa.app.ui.feed.blog.interactor.BlogMVPInteractor
import com.rifkaannisa.app.ui.feed.blog.view.BlogMVPView

/**
 * Created by jyotidubey on 13/01/18.
 */
interface BlogMVPPresenter<V : BlogMVPView, I : BlogMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}