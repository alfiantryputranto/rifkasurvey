package com.rifkaannisa.app.ui.rate

import com.rifkaannisa.app.ui.rate.interactor.RateUsInteractor
import com.rifkaannisa.app.ui.rate.interactor.RateUsMVPInterator
import com.rifkaannisa.app.ui.rate.presenter.RateUsMVPPresenter
import com.rifkaannisa.app.ui.rate.presenter.RateUsPresenter
import com.rifkaannisa.app.ui.rate.view.RateUsDialogMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by jyotidubey on 15/01/18.
 */
@Module
class RateUsFragmentModule {

    @Provides
    internal fun provideRateUsInteractor(interactor: RateUsInteractor): RateUsMVPInterator = interactor

    @Provides
    internal fun provideRateUsPresenter(presenter: RateUsPresenter<RateUsDialogMVPView, RateUsMVPInterator>)
            : RateUsMVPPresenter<RateUsDialogMVPView, RateUsMVPInterator> = presenter
}