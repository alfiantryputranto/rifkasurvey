package com.rifkaannisa.app.ui.feed.opensource.presenter

import com.rifkaannisa.app.ui.base.presenter.MVPPresenter
import com.rifkaannisa.app.ui.feed.opensource.interactor.OpenSourceMVPInteractor
import com.rifkaannisa.app.ui.feed.opensource.view.OpenSourceMVPView

/**
 * Created by jyotidubey on 14/01/18.
 */
interface OpenSourceMVPPresenter<V : OpenSourceMVPView, I : OpenSourceMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
}