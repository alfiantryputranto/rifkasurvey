package com.rifkaannisa.app.ui.login.interactor

import com.rifkaannisa.app.data.network.LoginResponse
import com.rifkaannisa.app.ui.base.interactor.MVPInteractor
import com.rifkaannisa.app.util.AppConstants
import io.reactivex.Observable

/**
 * Created by jyotidubey on 10/01/18.
 */
interface LoginMVPInteractor : MVPInteractor {

    fun doServerLoginApiCall(username: String, password: String): Observable<LoginResponse>

    fun updateUserInSharedPref(loginResponse: LoginResponse, loggedInMode: AppConstants.LoggedInMode)

}