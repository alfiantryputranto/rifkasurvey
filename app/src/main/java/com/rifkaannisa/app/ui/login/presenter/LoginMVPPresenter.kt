package com.rifkaannisa.app.ui.login.presenter

import com.rifkaannisa.app.ui.base.presenter.MVPPresenter
import com.rifkaannisa.app.ui.login.interactor.LoginMVPInteractor
import com.rifkaannisa.app.ui.login.view.LoginMVPView

/**
 * Created by jyotidubey on 10/01/18.
 */
interface LoginMVPPresenter<V : LoginMVPView, I : LoginMVPInteractor> : MVPPresenter<V, I> {

    fun onServerLoginClicked(username: String, password: String)

}