package com.rifkaannisa.app.ui.feed.opensource.view

import com.rifkaannisa.app.data.network.OpenSource
import com.rifkaannisa.app.ui.base.view.MVPView

/**
 * Created by jyotidubey on 14/01/18.
 */
interface OpenSourceMVPView : MVPView {
    fun displayOpenSourceList(blogs: List<OpenSource>?)

}