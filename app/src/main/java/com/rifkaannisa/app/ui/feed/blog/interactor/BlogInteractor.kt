package com.rifkaannisa.app.ui.feed.blog.interactor

import com.rifkaannisa.app.data.network.ApiHelper
import com.rifkaannisa.app.data.preferences.PreferenceHelper
import com.rifkaannisa.app.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by jyotidubey on 13/01/18.
 */
class BlogInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), BlogMVPInteractor {

    override fun getBlogList() = apiHelper.getBlogApiCall()

}