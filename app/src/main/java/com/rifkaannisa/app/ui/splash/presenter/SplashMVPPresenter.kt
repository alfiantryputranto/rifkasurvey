package com.rifkaannisa.app.ui.splash.presenter

import com.rifkaannisa.app.ui.base.presenter.MVPPresenter
import com.rifkaannisa.app.ui.splash.interactor.SplashMVPInteractor
import com.rifkaannisa.app.ui.splash.view.SplashMVPView

/**
 * Created by jyotidubey on 04/01/18.
 */
interface SplashMVPPresenter<V : SplashMVPView, I : SplashMVPInteractor> : MVPPresenter<V,I>