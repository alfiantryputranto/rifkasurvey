package com.rifkaannisa.app.ui.login.view

import com.rifkaannisa.app.ui.base.view.MVPView

/**
 * Created by jyotidubey on 10/01/18.
 */
interface LoginMVPView : MVPView {

    fun showValidationMessage(errorCode: Int)
    fun openMainActivity()
}