package com.rifkaannisa.app.ui.rate.presenter

import com.rifkaannisa.app.ui.base.presenter.MVPPresenter
import com.rifkaannisa.app.ui.rate.interactor.RateUsMVPInterator
import com.rifkaannisa.app.ui.rate.view.RateUsDialogMVPView

/**
 * Created by jyotidubey on 15/01/18.
 */
interface RateUsMVPPresenter<V : RateUsDialogMVPView, I : RateUsMVPInterator> : MVPPresenter<V, I> {

    fun onLaterOptionClicked() : Unit?
    fun onSubmitOptionClicked() : Unit?
}