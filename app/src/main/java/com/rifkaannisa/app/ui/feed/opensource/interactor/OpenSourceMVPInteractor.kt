package com.rifkaannisa.app.ui.feed.opensource.interactor

import com.rifkaannisa.app.data.network.OpenSourceResponse
import com.rifkaannisa.app.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by jyotidubey on 14/01/18.
 */
interface OpenSourceMVPInteractor : MVPInteractor {

    fun getOpenSourceList(): Observable<OpenSourceResponse>
}