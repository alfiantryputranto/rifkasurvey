package com.rifkaannisa.app.ui.rate.interactor

import com.rifkaannisa.app.data.network.ApiHelper
import com.rifkaannisa.app.data.preferences.PreferenceHelper
import com.rifkaannisa.app.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by jyotidubey on 15/01/18.
 */
class RateUsInteractor @Inject internal constructor(apiHelper: ApiHelper, preferenceHelper: PreferenceHelper) : BaseInteractor(apiHelper = apiHelper, preferenceHelper = preferenceHelper), RateUsMVPInterator {

    override fun submitRating() {}
}