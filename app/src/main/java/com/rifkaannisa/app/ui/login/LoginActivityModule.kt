package com.rifkaannisa.app.ui.login

import com.rifkaannisa.app.ui.login.interactor.LoginInteractor
import com.rifkaannisa.app.ui.login.interactor.LoginMVPInteractor
import com.rifkaannisa.app.ui.login.presenter.LoginMVPPresenter
import com.rifkaannisa.app.ui.login.presenter.LoginPresenter
import com.rifkaannisa.app.ui.login.view.LoginMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by jyotidubey on 10/01/18.
 */
@Module
class LoginActivityModule {

    @Provides
    internal fun provideLoginInteractor(interactor: LoginInteractor): LoginMVPInteractor = interactor

    @Provides
    internal fun provideLoginPresenter(presenter: LoginPresenter<LoginMVPView, LoginMVPInteractor>)
            : LoginMVPPresenter<LoginMVPView, LoginMVPInteractor> = presenter

}