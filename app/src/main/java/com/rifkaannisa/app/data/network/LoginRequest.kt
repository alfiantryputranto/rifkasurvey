package com.rifkaannisa.app.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jyotidubey on 11/01/18.
 */
class LoginRequest {

  data class ServerLoginRequest internal constructor(
    @Expose
    @SerializedName("username") internal val username: String,
    @Expose
    @SerializedName("password") internal val password: String
  )

}