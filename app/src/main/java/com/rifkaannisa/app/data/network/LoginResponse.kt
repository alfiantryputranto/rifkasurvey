package com.rifkaannisa.app.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jyotidubey on 11/01/18.
 */
data class LoginResponse(@Expose
                         @SerializedName("id")
                         var userId: Long? = null,

                         @Expose
                         @SerializedName("api_token")
                         var accessToken: String? = null,

                         @Expose
                         @SerializedName("username")
                         var userName: String? = null,

                         @Expose
                         @SerializedName("fullname")
                         var fullName: String? = null,

                         @Expose
                         @SerializedName("email")
                         var email: String? = null,

                         @Expose
                         @SerializedName("role")
                         var role: String? = null,

                         @Expose
                         @SerializedName("lembaga")
                         var lembaga: String? = null)


