package com.rifkaannisa.app.data.network

import com.rifkaannisa.app.BuildConfig

/**
 * Created by jyotidubey on 11/01/18.
 */
object ApiEndPoint {

    val ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL + "/login"
    val ENDPOINT_LOGOUT = BuildConfig.BASE_URL + "/logout"

}