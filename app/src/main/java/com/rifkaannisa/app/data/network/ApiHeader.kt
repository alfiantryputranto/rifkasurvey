package com.rifkaannisa.app.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.rifkaannisa.app.di.ApiKeyInfo
import javax.inject.Inject

/**
 * Created by jyotidubey on 11/01/18.
 */
class ApiHeader @Inject constructor(internal val protectedApiHeader: ProtectedApiHeader) {


    class ProtectedApiHeader @Inject constructor(@Expose
                                                 @SerializedName("access_token") val accessToken: String?)

}