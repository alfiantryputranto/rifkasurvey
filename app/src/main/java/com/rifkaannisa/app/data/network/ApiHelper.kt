package com.rifkaannisa.app.data.network

import io.reactivex.Observable

/**
 * Created by jyotidubey on 04/01/18.
 */
interface ApiHelper {

    fun performServerLogin(request: LoginRequest.ServerLoginRequest): Observable<LoginResponse>

    fun performLogoutApiCall(): Observable<LogoutResponse>
  fun getBlogApiCall(): Observable<BlogResponse>
  abstract fun getOpenSourceApiCall(): Observable<OpenSourceResponse>

}