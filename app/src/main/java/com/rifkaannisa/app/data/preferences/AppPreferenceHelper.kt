package com.rifkaannisa.app.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.content.edit
import com.rifkaannisa.app.di.PreferenceInfo
import com.rifkaannisa.app.util.AppConstants
import javax.inject.Inject

/**
 * Created by jyotidubey on 04/01/18.
 */
class AppPreferenceHelper @Inject constructor(
  context: Context,
  @PreferenceInfo private val prefFileName: String
) : PreferenceHelper {
  companion object {
    private val PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE"
    private val PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID"
    private val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    private val PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME"
    private val PREF_KEY_CURRENT_USER_FULLNAME = "PREF_KEY_CURRENT_USER_FULLNAME"
    private val PREF_KEY_CURRENT_USER_ROLE = "PREF_KEY_CURRENT_USER_ROLE"
    private val PREF_KEY_CURRENT_USER_LEMBAGA = "PREF_KEY_CURRENT_USER_LEMBAGA"
    private val PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL"
  }

  private val mPrefs: SharedPreferences =
    context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

  override fun getCurrentUserLoggedInMode() = mPrefs.getInt(
      PREF_KEY_USER_LOGGED_IN_MODE, AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type
  )

  override fun getCurrentUserName(): String = mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, "ABC")

  override fun setCurrentUserName(userName: String?) =
    mPrefs.edit { putString(PREF_KEY_CURRENT_USER_NAME, userName) }

  override fun getCurrentUserEmail(): String =
    mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, "abc@gmail.com")

  override fun setCurrentUserEmail(email: String?) =
    mPrefs.edit { putString(PREF_KEY_CURRENT_USER_EMAIL, email) }

  override fun getAccessToken(): String = mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "")

  override fun setAccessToken(accessToken: String?) =
    mPrefs.edit { putString(PREF_KEY_ACCESS_TOKEN, accessToken) }

  override fun getCurrentUserId(): Long? {
    val userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX)
    return when (userId) {
      AppConstants.NULL_INDEX -> null
      else -> userId
    }
  }

  override fun setCurrentUserId(userId: Long?) {
    val id = userId ?: AppConstants.NULL_INDEX
    mPrefs.edit { putLong(PREF_KEY_CURRENT_USER_ID, id) }
  }

  override fun setCurrentUserLoggedInMode(mode: AppConstants.LoggedInMode) {
    mPrefs.edit { putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.type) }
  }

  override fun setCurrentUserFullName(fullname: String?) =
    mPrefs.edit { putString(PREF_KEY_CURRENT_USER_FULLNAME, fullname) }

  override fun getCurrentUserFullName(): String = mPrefs.getString(
      PREF_KEY_CURRENT_USER_FULLNAME, ""
  )

  override fun setCurrentUserRole(role: String?) =
    mPrefs.edit { putString(PREF_KEY_CURRENT_USER_ROLE, role) }

  override fun getCurrentUserRole(): String = mPrefs.getString(
      PREF_KEY_CURRENT_USER_ROLE, ""
  )

  override fun setCurrentUserLembaga(lembaga: String?) =
    mPrefs.edit { putString(PREF_KEY_CURRENT_USER_LEMBAGA, lembaga) }

  override fun getCurrentUserLembaga(): String = mPrefs.getString(
      PREF_KEY_CURRENT_USER_LEMBAGA, ""
  )

}