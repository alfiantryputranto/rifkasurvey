package com.rifkaannisa.app.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.rifkaannisa.app.BuildConfig
import com.rifkaannisa.app.data.database.AppDatabase
import com.rifkaannisa.app.data.database.repository.options.OptionsRepo
import com.rifkaannisa.app.data.database.repository.options.OptionsRepository
import com.rifkaannisa.app.data.database.repository.questions.QuestionRepo
import com.rifkaannisa.app.data.database.repository.questions.QuestionRepository
import com.rifkaannisa.app.data.network.ApiHeader
import com.rifkaannisa.app.data.network.ApiHelper
import com.rifkaannisa.app.data.network.AppApiHelper
import com.rifkaannisa.app.data.preferences.AppPreferenceHelper
import com.rifkaannisa.app.data.preferences.PreferenceHelper
import com.rifkaannisa.app.di.ApiKeyInfo
import com.rifkaannisa.app.di.PreferenceInfo
import com.rifkaannisa.app.util.AppConstants
import com.rifkaannisa.app.util.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

/**
 * Created by jyotidubey on 05/01/18.
 */
@Module
class AppModule {

  @Provides
  @Singleton
  internal fun provideContext(application: Application): Context = application

  @Provides
  @Singleton
  internal fun provideAppDatabase(context: Context): AppDatabase =
    Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.APP_DB_NAME).build()

  @Provides
  @PreferenceInfo
  internal fun provideprefFileName(): String = AppConstants.PREF_NAME

  @Provides
  @Singleton
  internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper =
    appPreferenceHelper

  @Provides
  @Singleton
  internal fun provideProtectedApiHeader(preferenceHelper: PreferenceHelper)
      : ApiHeader.ProtectedApiHeader =
    ApiHeader.ProtectedApiHeader(accessToken = preferenceHelper.getAccessToken())

  @Provides
  @Singleton
  internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

  @Provides
  @Singleton
  internal fun provideQuestionRepoHelper(appDatabase: AppDatabase): QuestionRepo =
    QuestionRepository(appDatabase.questionsDao())

  @Provides
  @Singleton
  internal fun provideOptionsRepoHelper(appDatabase: AppDatabase): OptionsRepo =
    OptionsRepository(appDatabase.optionsDao())

  @Provides
  internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

  @Provides
  internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()

}