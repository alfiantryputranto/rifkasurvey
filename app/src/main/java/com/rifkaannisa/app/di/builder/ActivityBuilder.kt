package com.rifkaannisa.app.di.builder

import com.rifkaannisa.app.ui.about.AboutFragmentProvider
import com.rifkaannisa.app.ui.feed.blog.BlogFragmentProvider
import com.rifkaannisa.app.ui.feed.opensource.OpenSourceFragmentProvider
import com.rifkaannisa.app.ui.feed.view.FeedActivity
import com.rifkaannisa.app.ui.login.LoginActivityModule
import com.rifkaannisa.app.ui.login.view.LoginActivity
import com.rifkaannisa.app.ui.main.MainActivityModule
import com.rifkaannisa.app.ui.main.view.MainActivity
import com.rifkaannisa.app.ui.rate.RateUsDialogFragmentProvider
import com.rifkaannisa.app.ui.splash.SplashActivityModule
import com.rifkaannisa.app.ui.splash.view.SplashMVPActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by jyotidubey on 05/01/18.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashMVPActivity

    @ContributesAndroidInjector(modules = [(MainActivityModule::class), (RateUsDialogFragmentProvider::class), (AboutFragmentProvider::class)])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(LoginActivityModule::class)])
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(BlogFragmentProvider::class), (OpenSourceFragmentProvider::class)])
    abstract fun bindFeedActivity(): FeedActivity

}